#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header *b, const char *fmt, ...);

void debug(const char *fmt, ...);

extern inline block_size size_from_capacity(block_capacity cap);//выдаёт вместимость блока

extern inline block_capacity capacity_from_size(block_size sz);//выдаёт размер блока

//Блок достаточно большой?
static bool block_is_big_enough(size_t query, struct block_header *block) { return block->capacity.bytes >= query; }

//Считает страницы количество страниц
static size_t pages_count(size_t mem) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }

//????
static size_t round_pages(size_t mem) { return getpagesize() * pages_count(mem); }

//Инициализирует блок пула в памяти
static void block_init(void *restrict addr, block_size block_sz, void *restrict next) {
    *((struct block_header *) addr) = (struct block_header) {
            .next = next,
            .capacity = capacity_from_size(block_sz),
            .is_free = true
    };
}

static size_t region_actual_size(size_t query) { return size_max(round_pages(query), REGION_MIN_SIZE); }

extern inline bool region_is_invalid(const struct region *r);


static void *map_pages(void const *addr, size_t length, int additional_flags) {
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(void const *addr, size_t query) {
    size_t region_size = region_actual_size(query + offsetof(struct block_header, contents));
    void *map_addr = map_pages(addr, region_size, MAP_FIXED_NOREPLACE);

    if (map_addr == MAP_FAILED || !map_addr) {
        map_addr = map_pages(addr, region_size, 0);
        if (map_addr == MAP_FAILED || !map_addr) {
            return REGION_INVALID;
        }
    }

    block_init(map_addr, (block_size) {region_size}, NULL);
    return (struct region) {.addr = map_addr, .size = region_size, .extends = (addr == map_addr)};
}

static void *block_after(struct block_header const *block);

void *heap_init(size_t initial) {
    const struct region region = alloc_region(HEAP_START, initial);
    if (region_is_invalid(&region)) return NULL;

    return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable(struct block_header *restrict block, size_t query) {
    return block->is_free &&
           query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big(struct block_header *block, size_t query) {
    if (!block) {
        return false;
    }
    if (block_splittable(block, query)) {
        block_size old_size;
        block_size new_size;
        block_size size;

        old_size = size_from_capacity(block->capacity);
        struct block_header *next = block->next;
        new_size.bytes = query + offsetof(struct block_header, contents);
        block_init(block, new_size, NULL);

        size.bytes = old_size.bytes - size_from_capacity(block->capacity).bytes;
        block_init((uint8_t *) (block) + size_from_capacity(block->capacity).bytes, size, next);
        block->next = (struct block_header *) ((uint8_t *) (block) + size_from_capacity(block->capacity).bytes);
        return true;
    }
    return false;
}


/*  --- Слияние соседних свободных блоков --- */

static void *block_after(struct block_header const *block) {
    return (void *) (block->contents + block->capacity.bytes);
}

static bool blocks_continuous(
        struct block_header const *fst,
        struct block_header const *snd) {
    return (void *) snd == block_after(fst);
}

static bool mergeable(struct block_header const *restrict fst, struct block_header const *restrict snd) {
    if (!fst || !snd) return false;
    return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

static bool try_merge_with_next(struct block_header *block) {
    struct block_header *buf = block->next;
    if (buf && mergeable(block, block->next)) {
        block->capacity.bytes += size_from_capacity(buf->capacity).bytes;
        block->next = buf->next;
        return true;
    }
    return false;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
    enum {
        BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED
    } type;
    struct block_header *block;
};


static struct block_search_result find_good_or_last(struct block_header *restrict block, size_t sz) {

    struct block_header *current = block;
    struct block_header *last = block;

    while (current) {
        while (try_merge_with_next(current));
        if (block_is_big_enough(sz, current) && current->is_free) {
            struct block_search_result result;
            result.type = BSR_FOUND_GOOD_BLOCK;
            result.block = current;
            return result;
        }
        last = current;
        current = current->next;
    }
    struct block_search_result result;
    result.type = BSR_REACHED_END_NOT_FOUND;
    result.block = last;
    return result;
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header *block) {
    //проверка на NULL не требуется, т.к. это делается в следующих методах
    struct block_search_result new_block = find_good_or_last(block, query);
    if (new_block.type == BSR_FOUND_GOOD_BLOCK) {
        split_if_too_big(new_block.block, query);
        new_block.block->is_free = false;
    }

    return new_block;
}


static struct block_header *grow_heap(struct block_header *restrict last, size_t query) {
    if (!last) {
        return NULL;
    }
    block_size size = size_from_capacity(last->capacity);
    void *addr = (void *) ((uint8_t *) last + size.bytes);
    struct region new_reg = alloc_region(addr, query);
    if (region_is_invalid(&new_reg)) return NULL;
    last->next = new_reg.addr;
    if (try_merge_with_next(last)) return last;
    return last->next;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header *memalloc(size_t query, struct block_header *heap_start) {
    if (!heap_start) {
        return NULL;
    }
    size_t size = size_max(query, BLOCK_MIN_CAPACITY);
    struct block_search_result result = try_memalloc_existing(size, heap_start);
    if (result.type == BSR_REACHED_END_NOT_FOUND) {
        struct block_header *new = grow_heap(result.block, size);
        if (!new) return NULL;
        result = try_memalloc_existing(size, new);
    }
    if (result.type != BSR_FOUND_GOOD_BLOCK) return NULL;
    return result.block;
}

void *_malloc(size_t query) {
    struct block_header *const addr = memalloc(query, (struct block_header *) HEAP_START);
    if (addr) return addr->contents;
    else return NULL;
}

static struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(struct block_header, contents));
}

void _free(void *mem) {
    if (!mem) return;
    struct block_header *header = block_get_header(mem);
    header->is_free = true;
    while (header) {
        try_merge_with_next(header);
        header = header->next;
    }

}
