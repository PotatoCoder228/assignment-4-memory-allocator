//
// Created by potato_coder on 09.12.22.
//
#define _DEFAULT_SOURCE

#include "mem.h"
#include "mem_internals.h"
#include "stdlib.h"
#include "tests.h"
#include "time.h"
#include "util.h"
#include <stdio.h>

static char *get_format_time_now() {
    time_t now = time(0);
    char *time = ctime(&now);
    char *result_time = malloc(sizeof(char) * 27);
    result_time[0] = '[';
    result_time[25] = ']';
    result_time[26] = '\0';
    for (int i = 1; i <= 24; i++) {
        result_time[i] = time[i - 1];
    }
    return result_time;
}

static void print_log(FILE *stream, char *info) {
    char *time = get_format_time_now();
    fprintf(stream, "%s%s%s%s%s", "\n", time, "[", info, "]\n");
}

static void free_heap(struct block_header **start_block) {
    struct block_header *current = *start_block;
    while (current != NULL) {
        if (!current->is_free) {
            _free((void *) (((uint8_t *) current) + offsetof(struct block_header, contents)));
            current = current->next;
        }
    }
}

static void init_heap_test() {
    print_log(stderr, "Начало теста на создание кучи");
    void *heap = heap_init(10000);
    struct block_header *start_block = (struct block_header *) heap;
    if (heap == NULL) {
        err("Ошибка: кучу не удалось инициализировать");
    } else {
        debug_heap(stderr, heap);
    }
    free_heap(&start_block);
}

static void mem_allocate_test() {
    print_log(stderr, "Начало теста на выделение памяти в куче");
    void *heap = heap_init(10000);
    struct block_header *start_block = (struct block_header *) heap;
    void *memory = _malloc(1500);
    if (memory == NULL) {
        err("Ошибка: тест не пройден");
    }
    debug_heap(stderr, heap);
    if ((*start_block).capacity.bytes != 1000 || (*start_block).is_free) {
        err( "Ошибка: тест не пройден");
    }
    print_log(stderr, "Память успешно выделена");
    free_heap(&start_block);
}

static void one_block_free_test() {
    print_log(stderr, "Начало теста с освобождением 1 блока.");
    void *heap = heap_init(10000);
    struct block_header *start_block = (struct block_header *) heap;
    void *memory = _malloc(1500);
    _malloc(1500);
    _free(memory);
    debug_heap(stderr, heap);
    if (!((struct block_header *) (((uint8_t *) memory) - offsetof(struct block_header, contents)))->is_free) {
        err( "Ошибка: тест с освобождением 1 блока не пройден");
    }
    print_log(stderr, "Тест с освобождением 1 блока успешно пройден.");
    free_heap(&start_block);
}

static void two_blocks_free_test() {
    print_log(stderr, "Начало теста на освобождение памяти для 2 блоков в куче.");
    void *heap = heap_init(10000);
    struct block_header *start_block = (struct block_header *) heap;
    void *memory1 = _malloc(1000);
    void *memory2 = _malloc(1000);
    _free(memory1);
    _free(memory2);
    debug_heap(stderr, heap);
    if (!((struct block_header *) (((uint8_t *) memory1) - offsetof(struct block_header, contents)))->is_free ||
        !((struct block_header *) (((uint8_t *) memory2) - offsetof(struct block_header, contents)))->is_free) {
        err("Ошибка: тест с освобождением 2 блоков в куче провален.");
    }
    print_log(stderr, "Тест с освобождением 2 блоков успешно пройден.");
    free_heap(&start_block);
}

static void new_region_extends_old_test() {
    print_log(stderr, "Начало теста на расширение региона.\n");

    void *heap = heap_init(10000);
    struct block_header *start_block = (struct block_header *) heap;

    free_heap(&start_block);

    void *test_mem = _malloc(20000);

    debug_heap(stderr, heap);

    if(test_mem == NULL){
        err("Ошибка: тест на расширение региона провален.");
    }

    if (((struct block_header *) (((uint8_t *) test_mem) - offsetof(struct block_header, contents))) != heap ||
        ((struct block_header *) (((uint8_t *) test_mem) - offsetof(struct block_header, contents)))->is_free ||
        ((struct block_header *) (((uint8_t *) test_mem) - offsetof(struct block_header, contents)))->capacity.bytes !=
        20000) {
        err("Ошибка: тест на расширение региона провален.");
    }
    print_log(stderr, "Тест на расширение региона успешно пройден.");
    free_heap(&start_block);
}

static struct block_header *last_block(struct block_header **start_block) {
    for (struct block_header *last = *start_block;; last = last->next) {
        if (last->next == NULL) {
            return last;
        }
    }
}

static void new_region_another_place_test() {
    print_log(stderr, "Тест на выделение региона в другом месте.\n");

    void *heap = heap_init(10000);
    struct block_header *start_block = (struct block_header *) heap;

    void *test_taken_mem = mmap(heap, 1000, PROT_READ | PROT_WRITE,
                                MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE,
                                0, 0);

    print_log(stderr, "Адрес памяти:");
    print_log(stderr, test_taken_mem);

    void *test_mem = _malloc(40000);

    debug_heap(stderr, heap);

    if (test_mem == (*start_block).next || test_mem == test_taken_mem ||
        ((struct block_header *) (((uint8_t *) test_mem) - offsetof(struct block_header, contents)))->capacity.bytes !=
        40000 ||
        ((struct block_header *) (((uint8_t *) test_mem) - offsetof(struct block_header, contents)))->is_free) {
        err("Ошибка: новый регион не был выделен.");
    }
    print_log(stderr, "Тест на выделение нового региона в другом месте успешно пройден.");
    free_heap(&start_block);
}


void run_tests() {
    print_log(stderr, "<----- Начало общего теста функций ----->");
    init_heap_test();
    mem_allocate_test();
    one_block_free_test();
    two_blocks_free_test();
    new_region_extends_old_test();
    new_region_another_place_test();
    print_log(stderr, "<----- Все тесты пройдены успешно ----->");
}
