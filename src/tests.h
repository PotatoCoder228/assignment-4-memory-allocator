//
// Created by potato_coder on 09.12.22.
//

#ifndef MEMORY_ALLOCATOR_TESTS_H
#define MEMORY_ALLOCATOR_TESTS_H

#include "mem.h"
#include "mem_internals.h"

void run_tests();

#endif //MEMORY_ALLOCATOR_TESTS_H
